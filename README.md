# File System Manager

File System Manager is a Java console application where you can get information about and manipulate files.

## Installation

You need JDK to run this application.

## How to run
With .jar-file:
```bash
cd out
java -jar ConsoleProgram.jar
```

With .class file:
```bash
cd out
java main.java.Program
```

## How to compile
![Compilation](/screenshot/Compiled.png)
