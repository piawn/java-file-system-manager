package main.java.command_options;

import java.util.Scanner;

public class CommandOption {

    private Scanner sc = new Scanner(System.in);

    public int viewMenu() {
        System.out.println("1: Show all files");
        System.out.println("2: Show files based on extention");
        System.out.println("3: Get information about dracula.txt");
        System.out.println("4: Search for word in dracula.txt");
        System.out.println("Press 'q' to quit.");

        int option = 0;

        while (option == 0) {
            if (sc.hasNextInt()) {
                option = sc.nextInt();
            } else if (sc.hasNext()) {
                String invalidInput = sc.next();
                if (invalidInput.equals("q")) {
                    option = -1;
                    closeMenu();
                } else {
                    System.out.println("Your input '" + invalidInput + "' is not valid.");
                    System.out.println("Only numbers is valid input.");
                }
            }
        }
        return option;
    }

    public String viewFileExtensionMenu() {
        System.out.println("Show all files based on file extension:");
        System.out.println("1: .txt");
        System.out.println("2: .jpeg");
        System.out.println("3: .png");
        System.out.println("4: .jpg");
        System.out.println("5: .jfif");

        int option = 0;
        while (option == 0) {
            if (sc.hasNextInt()) {
                option = sc.nextInt();
            } else if (sc.hasNext()) {
                String invalidInput = sc.next();
                System.out.println("Your input '" + invalidInput + "' is not valid.");
                System.out.println("Only numbers is valid input.");
            }
        }
        switch (option) {
            case 1:
                return ".txt";
            case 2:
                return ".jpeg";
            case 3:
                return ".png";
            case 4:
                return ".jpg";
            case 5:
                return ".jfif";
            default:
                return "";
        }
    }

    public String viewSearchForWord() {
        System.out.println("What word would you like to search for?");
        String word = "";
        while (word.equals("")) {
            if (sc.hasNextLine()) {
                word = sc.nextLine();
            }
        }
        return word;
    }

    public void closeMenu() {
        System.out.println("Closing program...");
        sc.close();
    }
}
