package main.java.file_manipulation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;

import main.java.logging.Logging;

public class FileManipulation {

    private final String FILE_PATH = "../src/main/resources";
    private File directory = new File(FILE_PATH);
    private File[] files;
    private File dracula;
    private Logging log;

    public FileManipulation() {
        this.log = new Logging();
        try {
            files = directory.listFiles();
            dracula = new File(FILE_PATH + "/Dracula.txt");
        } catch (Exception e) {
            System.out.println("FileManipulation constructor: " + e);
        }
    }

    public String getAllFiles() {
        long startTime = System.nanoTime();
        String allFiles = "";
        for (File file : files) {
            allFiles += file.getName() + "\n";
        }

        long endTime = System.nanoTime();
        this.log.writeLog(allFiles, (endTime - startTime) / 1000000);

        return allFiles;
    }

    public String getFilesByExtension(String extention) {
        long startTime = System.nanoTime();
        System.out.println("Extension chosen: '" + extention + "'");

        String extensionFiles = "";
        File[] filesByExtension;

        try {
            FilenameFilter filter = new FilenameFilter() {
                @Override
                public boolean accept(File file, String name) {
                    return name.endsWith(extention);
                }
            };

            filesByExtension = directory.listFiles(filter);
            for (File file : filesByExtension) {
                extensionFiles += file.getName() + "\n";
            }
        } catch (Exception e) {
            System.out.println("getFilesByExtension(): " + e);
        }

        long endTime = System.nanoTime();
        this.log.writeLog(extensionFiles, (endTime - startTime) / 1000000);

        return extensionFiles;
    }

    public String getInformationAboutFile() {
        long startTime = System.nanoTime();
        String fileInformation = "";
        int lines = 0;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(dracula));
            while (reader.readLine() != null) {
                lines++;
            }
            reader.close();
            fileInformation = "Name: " + dracula.getName() + "\nSize in bytes: " + dracula.length()
                    + "\nNumber of lines: " + lines;
        } catch (Exception e) {
            System.out.println("getInformationAboutFile(): " + e);
        }

        long endTime = System.nanoTime();
        this.log.writeLog(fileInformation, (endTime - startTime) / 1000000);

        return fileInformation;
    }

    public String searchInFile(String searchWord) {
        long startTime = System.nanoTime();
        int count = 0;
        String[] words;
        String searchInformation = "";
        try {
            BufferedReader reader = new BufferedReader(new FileReader(dracula));
            String line = reader.readLine();
            while (line != null) {
                if (line.toLowerCase().contains(searchWord.toLowerCase())) {
                    words = line.split("\\P{L}+");
                    for (String word : words) {
                        if (word.equalsIgnoreCase(searchWord)) {
                            count++;
                        }
                    }
                }
                line = reader.readLine();
            }
            reader.close();
        } catch (IOException e) {
            System.out.println(e);
        }
        if (count > 0) {
            searchInformation = "The word '" + searchWord + "' was found " + count + " times in the file.\n";
        } else {
            searchInformation = "No matches for '" + searchWord + "' in the file.\n";
        }

        long endTime = System.nanoTime();
        this.log.writeLog(searchInformation, (endTime - startTime) / 1000000);

        return searchInformation;
    }
}
