package main.java.logging;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Logging {

    private final String LOCATION_PATH = "../log/log.txt";
    private File logFile = null;

    public Logging() {
        this.createFile();
    }

    public void createFile() {
        try {
            File loggingFile = new File(LOCATION_PATH);
            if (loggingFile.createNewFile()) {
                this.logFile = loggingFile;
            } else {
                this.logFile = new File(this.LOCATION_PATH);
            }
        } catch (IOException e) {
            System.out.println("createFile(): " + e);
        }
    }

    public void writeLog(String message, long executionTime) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        try {
            FileWriter writer = new FileWriter(this.logFile, true);
            writer.append("*** " + dtf.format(now) + " ***" + "\n" + message + "\n" + "The function took "
                    + executionTime + "ms to execute.\n\n");
            writer.close();
        } catch (IOException e) {
            System.out.println(e);
        }
    }
}
