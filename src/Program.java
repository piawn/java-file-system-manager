import main.java.command_options.CommandOption;
import main.java.file_manipulation.FileManipulation;

public class Program {

    private static CommandOption co = new CommandOption();
    private static FileManipulation fm = new FileManipulation();

    public static void main(String[] args) {

        runConsoleProgram();

    }

    public static void runConsoleProgram() {
        int option = co.viewMenu();
        while (option != -1) {
            switch (option) {
                case 1:
                    System.out.println(fm.getAllFiles());
                    break;
                case 2:
                    String extension = co.viewFileExtensionMenu();
                    if (!extension.equals("")) {
                        System.out.println(fm.getFilesByExtension(extension));
                    } else {
                        System.out.println("No valid option detected.");
                    }
                    break;
                case 3:
                    System.out.println(fm.getInformationAboutFile());
                    break;
                case 4:
                    String searchWord = co.viewSearchForWord();
                    if (!searchWord.equals("")) {
                        System.out.println(fm.searchInFile(searchWord));
                    } else {
                        System.out.println("No words detected.");
                    }
                    break;
                case -1:
                    co.closeMenu();
                    break;
                default:
                    System.out.println("Please choose a valid option.");
            }
            option = co.viewMenu();
        }
    }
}